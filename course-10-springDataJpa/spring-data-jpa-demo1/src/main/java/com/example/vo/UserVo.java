package com.example.vo;

import com.example.entity.Clazz;
import com.example.entity.User;
import lombok.Data;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Data
public class UserVo extends User {

    private Clazz clazz;
}
