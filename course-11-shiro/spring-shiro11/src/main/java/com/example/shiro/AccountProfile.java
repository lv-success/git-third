package com.example.shiro;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Data
public class AccountProfile implements Serializable {

    private long id;
    private String username;
    private String age;

}
