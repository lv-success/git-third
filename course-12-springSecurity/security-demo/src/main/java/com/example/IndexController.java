package com.example;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Controller
public class IndexController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @ResponseBody
    @GetMapping("/user")
    public Object user(Authentication authentication) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }
}
