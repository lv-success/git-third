package org.tio.examples.im.client.starter;

import java.io.IOException;

/**
 *
 * @author tanyaowu
 *
 */
public class ImClientStarter {

	/**
	 * @param args
	 *
	 * @author tanyaowu
	 * @throws IOException
	 * 2016年11月17日 下午5:59:24
	 *
	 */
	public static void main(String[] args) throws Exception {
		org.tio.examples.im.client.ImClientStarter.main(args);
	}

}
