package com.mq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MqController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    AmqpTemplate amqpTemplate;

    @Autowired
    ConfirmCallbackListener confirmCallbackListener;

    @GetMapping("/send")
    public void send () {

        //往队列发送信息
        rabbitTemplate.setConfirmCallback(confirmCallbackListener);
        rabbitTemplate.convertAndSend(RabbitConfig.SPRING_BOOT_QUEUE,"hello springboot rabbitmq");

        //往交换机发送信息
//        rabbitTemplate.convertAndSend(RabbitConfig.SPRING_BOOT_EXCHANGE, RabbitConfig.SPRING_BOOT_BIND_KEY, "hello springboot exchange message!!");
    }

}
