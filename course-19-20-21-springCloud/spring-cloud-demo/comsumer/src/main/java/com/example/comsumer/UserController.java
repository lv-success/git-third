package com.example.comsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@RestController
public class UserController {

    @Autowired
    LoadBalancerClient loadBalancerClient;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ProducterClient producterClient;

    @GetMapping("test")
    public String test() {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject("http://localhost:8088/order/1", String.class);
        return result;
    }

    @GetMapping("test2")
    public String test2() {
        RestTemplate restTemplate = new RestTemplate();

        ServiceInstance serviceInstance = loadBalancerClient.choose("PRODUCTER-CLIENT");
        String url = String.format("http://%s:%s/order/2", serviceInstance.getHost(), serviceInstance.getPort());
        System.out.println("-------->" + url);

        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    @GetMapping("test3")
    public String test3() {
        String result = restTemplate.getForObject("http://PRODUCTER-CLIENT/order/1", String.class);
        return result;
    }

    @GetMapping("test4")
    public String test4() {
        String result = producterClient.findById(4L);
        return result;
    }
}
