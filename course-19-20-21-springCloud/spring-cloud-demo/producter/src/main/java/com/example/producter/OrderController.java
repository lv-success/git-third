package com.example.producter;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@RestController
public class OrderController {

    @GetMapping("/order/{id}")
    public String findById(@PathVariable("id") Long id) {
        return "this is order-" + id;
    }

}
