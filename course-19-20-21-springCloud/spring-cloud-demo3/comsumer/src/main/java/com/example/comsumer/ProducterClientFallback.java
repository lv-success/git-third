package com.example.comsumer;

/**
 * @author 吕一明
 * @公众号 码客在线
 */

import org.springframework.stereotype.Component;
@Component
public class ProducterClientFallback implements ProducterClient{
    @Override
    public String findById(Long id) {
        return "fallback feign client - " + id;
    }
}
