package com.lfq;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
        Set<String> sentinels = new HashSet<>();
        sentinels.add("192.168.1.101:26379");
        sentinels.add("192.168.1.101:26380");
        sentinels.add("192.168.1.101:26381");

        JedisSentinelPool pool = new JedisSentinelPool("mymaster", sentinels);

        while (true) {
            Jedis jedis = null;
            try {
                jedis = pool.getResource();
                String ip = jedis.getClient().getHost();
                int port = jedis.getClient().getPort();

                String key = "k-" + new Random().nextInt(10000);
                String value = "v-" + new Random().nextInt(10000);
                jedis.set(key, value);

                System.out.println("当前主库：" + ip + ":" + port + "，" + key + " value is " + jedis.get(key));

                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(jedis != null) {
                    jedis.close();
                }
            }
        }

    }
}
