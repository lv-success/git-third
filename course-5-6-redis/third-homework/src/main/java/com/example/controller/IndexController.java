package com.example.controller;

/**
 * @author 吕一明
 * @公众号 码客在线
 */

import org.redisson.api.RLock;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
public class IndexController extends BaseController {

    @RequestMapping({"", "/", "/index"})
    public String index () {
        req.setAttribute("base", req.getContextPath());
        return "index";
    }

    @ResponseBody
    @GetMapping("/getLastRank")
    public Object getLastRank() {
        Set<ZSetOperations.TypedTuple> lastWeekRank = redisUtil.getZSetRank("last_week_rank", 0, 9);

        List<Map<String, Object>> hotPosts = new ArrayList<>();
        for (ZSetOperations.TypedTuple typedTuple : lastWeekRank) {

            Map<String, Object> map = new HashMap<>();
            map.put("comment_count", typedTuple.getScore());
            map.put("id", redisUtil.hget("rank_post_" + typedTuple.getValue(), "post:id"));
            map.put("title", redisUtil.hget("rank_post_" + typedTuple.getValue(), "post:title"));

            hotPosts.add(map);
        }
        return hotPosts;
    }

    @ResponseBody
    @GetMapping("/post/{id}")
    public Object view(@PathVariable Long id) {
        return postService.get(id);
    }

    @ResponseBody
    @GetMapping("/post/update/{id}")
    public Object update(@PathVariable Long id) {
        return postService.update(postService.get(id));
    }

    static int count = 0;

    @ResponseBody
    @GetMapping("/count")
    public void count() {
        RLock lock = redissonClient.getLock("count");
        lock.lock();
        try {
            IndexController.count++;
            System.out.println(Thread.currentThread().getId() + "------>" + IndexController.count);
        } finally {
            lock.unlock();
        }

    }
}
