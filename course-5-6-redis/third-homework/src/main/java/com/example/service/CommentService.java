package com.example.service;

import com.example.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 公众号：java思维导图
 * @since 2019-04-18
 */
public interface CommentService extends IService<Comment> {

}
