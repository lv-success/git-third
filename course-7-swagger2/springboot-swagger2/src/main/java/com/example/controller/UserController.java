package com.example.controller;


import com.example.entity.User;
import com.example.service.UserService;
import com.example.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lv-success
 * @since 2019-04-10
 */
@Api(value = "用户控制器", tags = "用户控制器")
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @ApiOperation("用户列表")
    @GetMapping("")
    public Object getUsers() {
        List<User> students = userService.list();
        return Result.succ(students);
    }

    @PostMapping("")
    public Object postUser(@RequestBody User student) {
        student.setCreated(new Date());
        student.setAvatar("");
        userService.saveOrUpdate(student);
        return Result.succ(null);
    }

    @GetMapping("/{id}")
    public Object getUser(@PathVariable("id") Long id) {
        User student = userService.getById(id);
        return Result.succ(student);
    }

    @PutMapping("/{id}")
    public Object putUser(@PathVariable("id") Long id, User user) {
        User tempUser = userService.getById(id);
        tempUser.setUsername(user.getUsername());
        tempUser.setWechat(user.getWechat());
        userService.updateById(tempUser);

        return Result.succ(null);
    }

    @DeleteMapping("/{id}")
    public Object deleteUser(@PathVariable Long id) {
        userService.removeById(id);
        return Result.succ(null);
    }

}
